package com.example.uapv1900102.tp1;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CountryAdapter extends ArrayAdapter<Country> {


    //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
    private LayoutInflater mInflater;

    ArrayList<Country> pays=new ArrayList<Country>();



    public CountryAdapter(Activity context, int textViewResourceId, ArrayList<Country> pays) {
        super(context, textViewResourceId,pays );
        this.mInflater = LayoutInflater.from(context);
    }


    public class CountryviewHolder{
        ImageView image;
        TextView nom;

        CountryviewHolder(View v){
            image = v.findViewById(R.id.imageview);
            nom = v.findViewById(R.id.countryname);
        }
    }



    public View getView(int position, View convertView, ViewGroup parent) {

        CountryviewHolder viewHolder =null ;

        if ( convertView== null)
        {
            convertView =  mInflater.inflate(R.layout.item, null, false);
            viewHolder = new CountryviewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else
        {
          viewHolder= (CountryviewHolder) convertView.getTag();
        }

        Country c = this.getItem(position);
        viewHolder.image.setImageResource(c.getmImgFile());
        viewHolder.nom.setText(c.getNom());

        return convertView;
    }
}
