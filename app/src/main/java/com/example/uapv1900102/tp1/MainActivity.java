package com.example.uapv1900102.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Country> pays = new ArrayList<Country>();

        pays.add(new Country("France",R.drawable.flag_of_france));
        pays.add(new Country("Allemagne",R.drawable.flag_of_germany));
        pays.add(new Country("Espagne",R.drawable.flag_of_spain));
        pays.add(new Country("Afrique du Sud",R.drawable.flag_of_south_africa));
        pays.add(new Country("États-Unis",R.drawable.flag_of_the_united_states));
        pays.add(new Country("Japon",R.drawable.flag_of_japan));

        mListView = (ListView)findViewById(R.id.list);

        CountryAdapter adapter = new CountryAdapter(this,R.layout.item, pays);


        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {

                Intent intent = new Intent(MainActivity.this,MainActivity2.class);

                Country p = pays.get(position);
                Integer f = p.getmImgFile();
                intent.putExtra("pays", p.getNom().toString());
                intent.putExtra("flag", f);

                startActivityForResult(intent,0);

            }

    });

    }
}

