package com.example.uapv1900102.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity2 extends AppCompatActivity {

    CountryList c = new CountryList();
    ImageView Drapeau ;
    EditText capitale ;
    EditText langue ;
    EditText monnaie ;
    EditText population ;
    EditText superficie ;
    Button button;
    Country pays;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();

        String  Nompays = intent.getStringExtra("pays");

        pays= c.getCountry(Nompays);


             Drapeau = (ImageView) findViewById(R.id.drapeau);
            capitale = (EditText) findViewById(R.id.editcapitale);
             langue = (EditText) findViewById(R.id.editlangue);
            monnaie = (EditText) findViewById(R.id.editmonnaie);
             population = (EditText) findViewById(R.id.editpopulation);
            superficie = (EditText) findViewById(R.id.editsuperficie);

         Integer flag = intent.getIntExtra("flag",R.drawable.flag_of_france);

        Drapeau.setBackgroundResource(flag);
        capitale.setText(pays.getmCapital());
            langue.setText(pays.getmLanguage());
            monnaie.setText(pays.getmCurrency());
             population.setText(Integer.toString(pays.getmPopulation()));
            superficie.setText(Integer.toString(pays.getmArea()));

            button = findViewById(R.id.button);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sauvegarder();

                    }
                    }
            );

    }


    public void sauvegarder(){
        //pays.setmPopulation(Integer.parseInt(population.getText().toString()));
        //pays.setmArea(Integer.parseInt(superficie.getText().toString()));
        pays.setmCapital(capitale.getText().toString());
        pays.setmCurrency(monnaie.getText().toString());
        pays.setmLanguage(langue.getText().toString());

        c.setHashMap(pays);
        finish();

    }

}
