package com.example.uapv1900102.tp1;

public class Country {
    private String nom;
    private String mCapital;
    private int mImgFile;
    private String mLanguage;
    private String mCurrency;
    private int mPopulation;
    private int mArea;

    public Country(String mCapital, int mImgFile, String mLanguage, String mCurrency, int mPopulation, int mArea) {
        this.mCapital = mCapital;
        this.mImgFile = mImgFile;
        this.mLanguage = mLanguage;
        this.mCurrency = mCurrency;
        this.mPopulation = mPopulation;
        this.mArea = mArea;
    }
    public Country(String nom,int mImgFile){
        this.nom=nom;
        this.mImgFile=mImgFile;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getmCapital() {
        return mCapital;
    }

    public void setmCapital(String mCapital) {
        this.mCapital = mCapital;
    }

    public int getmImgFile() {
        return mImgFile;
    }

    public void setmImgFile(int mImgFile) {
        this.mImgFile = mImgFile;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmCurrency() {
        return mCurrency;
    }

    public void setmCurrency(String mCurrency) {
        this.mCurrency = mCurrency;
    }

    public int getmPopulation() {
        return mPopulation;
    }

    public void setmPopulation(int mPopulation) {
        this.mPopulation = mPopulation;
    }

    public int getmArea() {
        return mArea;
    }

    public void setmArea(int mArea) {
        this.mArea = mArea;
    }

}
